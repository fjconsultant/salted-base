/srv/formulas:
  file.directory:
    - user: root
    - group: root
    - mode: 700
    - makedirs: True
epel:
  git.latest:
    - name: https://github.com/saltstack-formulas/epel-formula.git
    - target: /srv/formulas/epel-formula
    - force: True