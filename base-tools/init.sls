base-tools:
  include:
    - epel
  pkg.installed:
    - pkgs:
      - bind-utils
      - tcpdump
      - net-snmp
      - certbot
      - mtr
      - wget
      - iftop
      - net-tools
      - htop
      - perl
      - rsync
      - screen
      - python2-pip
      - iotop
      - git