snmpd:
  service:
    - running
    - enable: True
    - watch:
      - file: /etc/snmp/snmpd.conf
snmpd.conf:
  file.managed:
    - name: /etc/snmp/snmpd.conf
    - source: salt://snmpd/snmpd.conf