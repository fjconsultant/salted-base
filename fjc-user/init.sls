fjc-user:
  user.present:
    - fullname: FJ Consultant
    - shell: /bin/zsh
    - home: /home/fjc-user
    - groups: 
      - wheel
    - createhome: True
    - password: $1$MoX86bme$3ez4owLEXxTZfmcoO6sj2/
  ssh_auth.present:
    - user: fjc-user
    - enc: ssh-rsa
    - source: salt://ssh_keys/fjc.pub
