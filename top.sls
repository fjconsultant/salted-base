base:
  '*':
    - acpid
    - base-tools
    - firewalld
    - fjc-user
    - iptables
    - ntpd
    - snmpd
    - sshd
    - timezone
    - vmtoolsd
    - epel