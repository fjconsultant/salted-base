sshd:
  service:
    - running
    - enable: True
sshkeys:
  ssh_auth.present:
    - user: root
    - enc: ssh-rsa
    - source: salt://ssh_keys/fjc.pub
/etc/ssh/sshd_config:
  file.managed:
    - source: salt://sshd/sshd_config.conf